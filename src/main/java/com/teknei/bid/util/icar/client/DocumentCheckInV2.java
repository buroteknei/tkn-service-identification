
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocumentCheckInV2 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentCheckInV2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Image1" type="{http://IdCloud.iCarVision/WS/}DocumentImage"/>
 *         &lt;element name="Image2" type="{http://IdCloud.iCarVision/WS/}DocumentImage" minOccurs="0"/>
 *         &lt;element name="DocumentIdToMerge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentCheckInV2", propOrder = {
    "reference",
    "activity",
    "image1",
    "image2",
    "documentIdToMerge"
})
public class DocumentCheckInV2 {

    @XmlElement(name = "Reference", required = true, nillable = true)
    protected String reference;
    @XmlElement(name = "Activity")
    protected String activity;
    @XmlElement(name = "Image1", required = true, nillable = true)
    protected DocumentImage image1;
    @XmlElement(name = "Image2")
    protected DocumentImage image2;
    @XmlElement(name = "DocumentIdToMerge")
    protected String documentIdToMerge;

    /**
     * Obtiene el valor de la propiedad reference.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReference() {
        return reference;
    }

    /**
     * Define el valor de la propiedad reference.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReference(String value) {
        this.reference = value;
    }

    /**
     * Obtiene el valor de la propiedad activity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Define el valor de la propiedad activity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivity(String value) {
        this.activity = value;
    }

    /**
     * Obtiene el valor de la propiedad image1.
     * 
     * @return
     *     possible object is
     *     {@link DocumentImage }
     *     
     */
    public DocumentImage getImage1() {
        return image1;
    }

    /**
     * Define el valor de la propiedad image1.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentImage }
     *     
     */
    public void setImage1(DocumentImage value) {
        this.image1 = value;
    }

    /**
     * Obtiene el valor de la propiedad image2.
     * 
     * @return
     *     possible object is
     *     {@link DocumentImage }
     *     
     */
    public DocumentImage getImage2() {
        return image2;
    }

    /**
     * Define el valor de la propiedad image2.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentImage }
     *     
     */
    public void setImage2(DocumentImage value) {
        this.image2 = value;
    }

    /**
     * Obtiene el valor de la propiedad documentIdToMerge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentIdToMerge() {
        return documentIdToMerge;
    }

    /**
     * Define el valor de la propiedad documentIdToMerge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentIdToMerge(String value) {
        this.documentIdToMerge = value;
    }

}
