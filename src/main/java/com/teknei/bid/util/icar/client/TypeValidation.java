
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TypeValidation.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeValidation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Validation"/>
 *     &lt;enumeration value="Normal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeValidation")
@XmlEnum
public enum TypeValidation {

    @XmlEnumValue("Validation")
    VALIDATION("Validation"),
    @XmlEnumValue("Normal")
    NORMAL("Normal");
    private final String value;

    TypeValidation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeValidation fromValue(String v) {
        for (TypeValidation c: TypeValidation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
