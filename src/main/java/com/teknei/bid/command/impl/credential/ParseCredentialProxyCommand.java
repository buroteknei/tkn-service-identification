package com.teknei.bid.command.impl.credential;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.service.remote.ByPassCaller;
import com.teknei.bid.util.icar.IcarDataSingleton;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ParseCredentialProxyCommand implements Command {

    @Value("${tkn.config.overrideNames}")
    private String overrideNames;
    @Autowired
    private BidClieRepository clieRepository;

    @Autowired
    private ByPassCaller visionCaller;
    private static final Logger log = LoggerFactory.getLogger(ParseCredentialProxyCommand.class);
    private static CharsetEncoder asciiEncoder =
            Charset.forName("ISO-8859-1").newEncoder();


    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse response = new CommandResponse();
        JSONObject featureRequest = new JSONObject();
        JSONArray featureRequestArray = new JSONArray();
        JSONArray requestsArray = new JSONArray();
        featureRequestArray.put(featureRequest);
        featureRequest.put("type", "TEXT_DETECTION");
        for (byte[] contentByte : request.getFileContent()) {
            JSONObject jsonRequest = new JSONObject();
            String b64Img = null;
            b64Img = Base64Utils.encodeToString(contentByte);
            JSONObject imageRequest = new JSONObject();
            imageRequest.put("content", b64Img);
            jsonRequest.put("image", imageRequest);
            jsonRequest.put("features", featureRequestArray);
            requestsArray.put(jsonRequest);
        }
        JSONObject mainRequest = new JSONObject();
        mainRequest.put("requests", requestsArray);
        String mainStringRequest = mainRequest.toString();
        String responseVision = visionCaller.validateCredentials(mainStringRequest);
        JSONObject jsonVisionResponse = new JSONObject(responseVision);
        JSONArray jsonArrayResponse = jsonVisionResponse.getJSONArray("responses");
        List<String> listResponses = new ArrayList<>();
        for (int i = 0; i < jsonArrayResponse.length(); i++) {
            JSONObject jsonLocalResponse = jsonArrayResponse.getJSONObject(i);
            JSONObject fullTextResponse = jsonLocalResponse.getJSONObject("fullTextAnnotation");
            String finalStringResponse = fullTextResponse.getString("text");
            listResponses.add(finalStringResponse);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("scanId", UUID.randomUUID().toString());
        for (int i = 0; i < listResponses.size(); i++) {
            jsonObject.put("answer" + i, listResponses.get(i));
        }
        JSONObject compatibleAnwer = null;
        if (jsonObject.getString("answer0").contains("NACIONAL")) {
            compatibleAnwer = buildResponseCompatibleINE(jsonObject);
        }else if(jsonObject.getString("answer0").toUpperCase().contains("PASAPORTE") ||
                jsonObject.getString("answer0").toUpperCase().contains("PASA PORTE") ||
                jsonObject.getString("answer0").toUpperCase().contains("EXPEDI") ||
                jsonObject.getString("answer0").toUpperCase().contains("TITULAR")){
                compatibleAnwer = buildResponseCompatiblePasaporte(jsonObject);
        } else if (jsonObject.getString("answer0").contains("FEDERAL")) {
            compatibleAnwer = buildResponseCompatibleIFE(jsonObject);
        }else {
            compatibleAnwer = buildResponseCompatibleINE(jsonObject);
        }
        response.setDesc(compatibleAnwer.toString());
        response.setScanId(compatibleAnwer.getString("scanId"));
        response.setStatus(Status.CREDENTIALS_ICAR_OK);
        overrideNames(compatibleAnwer, request.getId());
        IcarDataSingleton.getInstance().getInformationMap().put(compatibleAnwer.getString("scanId"), compatibleAnwer.toString());
        return response;
    }

    private void overrideNames(JSONObject content, Long id) {
        if (overrideNames.equalsIgnoreCase("t")) {
            JSONObject internal = content.getJSONObject("document");
            BidClie bidClie = clieRepository.findOne(id);
            String name = internal.optString("name", null);
            String apellidoP = internal.optString("firstSurname", null);
            String apellidoM = internal.optString("secondSurname", null);
            boolean changes = false;
            if (apellidoP != null) {
                changes = true;
                bidClie.setApePate(apellidoP);
            }
            if (apellidoM != null) {
                changes = true;
                bidClie.setApeMate(apellidoM);
            }
            if (name != null) {
                changes = true;
                bidClie.setNomClie(name);
            }
            if (changes) {
                clieRepository.save(bidClie);
            }
        }
    }

    private JSONObject buildResponseCompatibleIFE(JSONObject source) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", "OK");
        jsonObject.put("scanId", source.getString("scanId"));
        JSONObject document = new JSONObject();
        document.put("claveElector", parseClaveElector(source.getString("answer0")));
        String ocr = parseOCRIfe(source.getString("answer1"));
        if (ocr == null || ocr.isEmpty()) {
            ocr = "N/A-" + source.getString("scanId").substring(0, 8);
        }
        document.put("CRC_SECTION", ocr);
        JSONObject namesObject = parseName(source.getString("answer0"));
        if (namesObject == null) {
            namesObject = parseNameFallback(source.getString("answer0"));
        }
        document.put("surname", new StringBuilder(namesObject.optString("apellidoP", "")).append(" ").append(namesObject.optString("apellidoM", ""))).toString();
        document.put("secondSurname", namesObject.optString("apellidoM", ""));
        document.put("firstSurname", namesObject.optString("apellidoP", ""));
        document.put("name", namesObject.optString("name", ""));
        document.put("address", parseAddress(source.getString("answer0")));
        String curp = parseCurp(source.getString("answer0"));
        if (curp == null || curp.isEmpty()) {
            curp = "N/A";
        }
        document.put("curp", curp);
        document.put("dateOfBirth", "");
        jsonObject.put("document", document);
        return jsonObject;
    }

    private JSONObject buildResponseCompatiblePasaporte(JSONObject source){
        return new PassportParser().parsePassport(source);
    }

    private JSONObject buildResponseCompatibleINE(JSONObject source) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", "OK");
        jsonObject.put("scanId", source.getString("scanId"));
        JSONObject document = new JSONObject();
        document.put("claveElector", parseClaveElector(source.getString("answer0")));
        JSONObject mrzObject = parseMrz(source.optString("answer1", ""));
        document.put("MRZ", mrzObject.optString("mrz"));
        String ocr = mrzObject.optString("ocr", null);
        if (ocr == null || ocr.isEmpty()) {
            ocr = "N/A-" + source.getString("scanId").substring(0, 8);
        }
        document.put("CRC_SECTION", ocr);
        JSONObject namesObject = parseName(source.getString("answer0"));
        if (namesObject == null) {
            namesObject = parseNameFallback(source.getString("answer0"));
        }
        document.put("surname", new StringBuilder(namesObject.optString("apellidoP", "")).append(" ").append(namesObject.optString("apellidoM", ""))).toString();
        document.put("secondSurname", namesObject.optString("apellidoM", ""));
        document.put("firstSurname", namesObject.optString("apellidoP", ""));
        document.put("name", namesObject.optString("name", ""));
        document.put("address", parseAddress(source.getString("answer0")));
        String curp = parseCurp(source.getString("answer0"));
        if (curp == null || curp.isEmpty()) {
            curp = "N/A";
        }
        document.put("curp", curp);
        document.put("dateOfBirth", parseBirth(source.optString("answer0")));
        jsonObject.put("document", document);
        return jsonObject;
    }

    private String parseOCRIfe(String content) {
        Pattern pattern = null;
        String regex = "\\d\\d\\d\\d\\d";
        StringBuilder sb = new StringBuilder(regex);
        pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        if (!matcher.find()) {
            return null;
        }
        for (int i = 5; i < 13; i++) {
            sb.append("\\d");
            pattern = Pattern.compile(sb.toString());
            matcher = pattern.matcher(content);
            if (matcher.find()) {
                if (i != 12) {
                    continue;
                } else {
                    int begin = matcher.start();
                    int end = matcher.end();
                    content = content.substring(begin, end);
                    content = replaceASCII(content);
                    return content;
                }
            } else {
                sb.deleteCharAt(sb.length() - 1);
                sb.deleteCharAt(sb.length() - 1);
                sb.deleteCharAt(sb.length() - 1);
                pattern = Pattern.compile(sb.toString());
                matcher = pattern.matcher(content);
                if (matcher.find()) {
                    int begin = matcher.start();
                    int end = matcher.end();
                    content = content.substring(begin, end);
                    content = replaceASCII(content);
                    return content;
                } else {
                    return null;
                }
            }

        }
        return null;
    }

    private String replaceASCII(String text) {
        String r = text.replaceAll("\\P{InBasic_Latin}", "");
        return r;
    }

    private JSONObject parseMrz(String content) {
        content = content.trim().replace(" ", "").trim();
        String toTest1 = "IDMEX";
        String toTest2 = "MEX";
        String toTest3 = "ID";
        JSONObject jsonObject = new JSONObject();
        //String toRemove2 = "INSTITUTO FEDERAL ELECTORAL";
        try {
            int index = 0;
            if (content.contains(toTest1)) {
                index = content.indexOf(toTest1);
                content = content.substring(index);
            } else if (content.contains(toTest2)) {
                index = content.indexOf(toTest2);
                content = content.substring(index - 2);
            } else if (content.contains(toTest3)) {
                index = content.indexOf(toTest3);
                content = content.substring(index);
            }
            content = content.replace("\n", "").trim();
            content = content.replace("&lt;", "<");
            content = content.trim();
            content = replaceASCII(content);
            jsonObject.put("mrz", content);
            String ocr = null;
            try {
                int position = content.indexOf("<<");
                if (position == 15) {
                    ocr = content.substring(17, 17 + 13);
                    ocr = ocr.trim();
                } else {
                    ocr = content.substring(15, 15 + 13);
                    ocr = ocr.trim();
                }
                ocr = replaceASCII(ocr);
                jsonObject.put("ocr", ocr);
            } catch (Exception e) {
                log.error("Could not extract OCR: {}", e.getMessage());
            }
            return jsonObject;
        } catch (Exception e) {
            log.error("Could not extract MRZ: {}", e.getMessage());
            log.error("Content: {}", content);
            return jsonObject;
        }
    }

    private String parseCurp(String content) {
        try {
            content = content.toUpperCase();
            String token1Name = "CURP";
            int index = content.indexOf(token1Name);
            content = content.substring(index);
            int end = content.indexOf("\n");
            content = content.substring(4, end);
            content = content.replace("\n", "");
            if (content.contains("AÑO DE REGISTRO")) {
                content = content.substring(0, content.indexOf("AÑO DE REGISTRO")).trim();
            } else if (content.contains("ANO DE REGISTRO")) {
                content = content.substring(0, content.indexOf("ANO DE REGISTRO")).trim();
            } else if (content.contains("REGISTRO")) {
                content = content.substring(0, content.indexOf("REGISTRO")).trim();
            }
            content = content.trim();
            content = replaceASCII(content);
            if (content.length() > 24) {
                content = content.substring(0, 23);
            }
            return content;
        } catch (Exception e) {
            log.error("Could not extract 'Curp' : ", e.getMessage());
            log.error("Content: {}", content);
            return "";
        }
    }


    private String parseClaveElector(String content) {
        String fallback = content;
        try {
            String token1Name = "CLAVE DE ELECTOR";
            int index = content.indexOf(token1Name);
            content = content.substring(index);
            int end = content.indexOf("\n");
            content = content.substring(16, end);
            content = content.replace("\n", "");
            content = content.trim();
            if (asciiEncoder.canEncode(content)) {
                return content;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Could not extract 'ClaveElector' : {} going fallback", e.getMessage());
            log.error("Content: {}", content);
            return parseClaveElectorFallback(fallback);
        }
    }

    private String parseClaveElectorFallback(String content) {
        try {
            String regex = "\\w{3,6}\\d{3,8}\\w{0,2}\\d{0,2}";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(content);
            if (matcher.find()) {
                content = content.substring(matcher.start(), matcher.end());
                return content;
            }
            return "NA";
        } catch (Exception e) {
            log.error("Could not extract 'ClaveElector' in fallback : ", e.getMessage());
            log.error("Content: {}", content);
            return "";
        }
    }


    private String parseAddress(String content) {
        content = content.toUpperCase();
        try {
            String token1Name = "DOMICILIO";
            String token1Name1 = "DOMILIC";
            String token1Name2 = "DOMICILICO";
            String token1Name3 = "DOMICILIC";
            String token1Name4 = "ICILI";
            String token1Name5 = "DOMICILI0";
            String token1Name6 = "OOMICILIO";
            String[] tokens1 = {token1Name, token1Name1, token1Name2, token1Name3, token1Name6, token1Name4, token1Name5};
            String[] tokenEnds = {"CLAVE DE ELECTOR", "ELECTOR", "LECTOR", "FOLIO", "FOLI0", "F0LIO", "F0LI0", "AÑO DE REGISTRO", "ANO DE REGISTRO", "ECHA DE NACRENTO", "FECHA DE NACIMIENTO", "FOIO", "SEXO"};
            String parsed = null;
            String birthRegex = "\\d{2}\\/\\d{2}\\/\\d{4}";
            Pattern birtPattern = Pattern.compile(birthRegex);
            for (String s : tokens1) {
                if (content.contains(s)) {
                    for (String end : tokenEnds) {
                        if (content.contains(end)) {
                            int tIndex = content.indexOf(end);
                            if (content.indexOf(s) < tIndex) {
                                parsed = parseInfo(content, s, end);
                            } else {
                                int tIndex2 = content.lastIndexOf(end);
                                if (content.indexOf(s) < tIndex2) {
                                    parsed = content.substring(content.indexOf(s), content.lastIndexOf(end));
                                } else {
                                    continue;
                                }
                            }
                            for (String se : tokenEnds) {
                                parsed = parsed.replace(se, "");
                            }
                            Matcher bMatcher = birtPattern.matcher(parsed);
                            if (bMatcher.find()) {
                                String birth = parsed.substring(bMatcher.start(), bMatcher.end());
                                parsed = parsed.replace(birth, "");
                            }
                            parsed = parsed.replace(s, "");
                            if (parsed.indexOf("\\n") < 5 && parsed.indexOf("\\n") != -1) {
                                parsed = parsed.substring(parsed.indexOf("\\n"));
                            } else if (parsed.indexOf("\n") < 5 && parsed.indexOf("\n") != -1) {
                                parsed = parsed.substring(parsed.indexOf("\n"));
                            }
                            parsed = parsed.trim();
                            if (parsed.startsWith("\n") || parsed.startsWith("\\n")) {
                                parsed = parsed.substring(2);
                            }
                            parsed = parsed.trim();
                            if (parsed.startsWith("\n") || parsed.startsWith("\\n")) {
                                parsed = parsed.substring(2);
                            }
                            parsed = parsed.trim();
                            if (parsed.startsWith("\n") || parsed.startsWith("\\n")) {
                                parsed = parsed.substring(2);
                            }
                            return parsed;
                        }
                    }
                }
            }
            if (parsed == null) {
                String regex = "\\w{3,6}\\d{3,8}\\w{0,2}\\d{0,2}";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(content);
                if (matcher.find()) {
                    for (String s : tokens1) {
                        if (content.contains(s)) {
                            parsed = content.substring(content.indexOf(s), matcher.end());
                            parsed = parsed.substring(0, parsed.lastIndexOf("\n"));
                            parsed = parsed.replace(s, "");
                            if (parsed.startsWith("\n") || parsed.startsWith("\\n")) {
                                parsed = parsed.substring(2);
                            }
                            return parsed;
                        }
                    }
                }
            }
            return parsed;
        } catch (Exception e) {
            log.error("Could not extract 'Address' : ", e.getMessage());
            log.error("Content: {}", content);
            return "";
        }
    }

    private String parseBirth(String content) {
        String regex = "\\d\\d\\/\\d\\d\\/\\d\\d\\d\\d";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            int begin = matcher.start();
            int end = matcher.end();
            return content.substring(begin, end);
        }
        return "";
    }

    private JSONObject parseNameFallback(String content) {
        try {
            content = content.toUpperCase();
            String token = "DOMICILIO";
            String token2 = "DOMICILICO";
            String token3 = "DOMICILIC";
            String token4 = "ICILI";
            if (content.contains(token)) {
                content = content.substring(0, content.indexOf(token));
            } else if (content.contains(token2)) {
                content = content.substring(0, content.indexOf(token2));
            } else if (content.contains(token3)) {
                content = content.substring(0, content.indexOf(token3));
            } else if (content.contains(token4)) {
                content = content.substring(0, content.indexOf(token4));
            }
            int limit = content.lastIndexOf("\n");
            content = content.substring(0, limit);
            String tokenName = "NOMBRE";
            String tokenName2 = "OMBRE";
            String tokenName3 = "MBRE";
            String tokenName4 = "NOMB";
            if (content.contains(tokenName)) {
                content = content.substring(content.indexOf(tokenName));
            } else if (content.contains(tokenName2)) {
                content = content.substring(content.indexOf(tokenName2));
            } else if (content.contains(tokenName3)) {
                content = content.substring(content.indexOf(tokenName3));
            } else if (content.contains(tokenName4)) {
                content = content.substring(content.indexOf(tokenName4));
            }
            int first = content.indexOf("\n");
            String namePreParsed = content.substring(first);
            String[] nameTokens = namePreParsed.split("\\n");
            String name = nameTokens[nameTokens.length - 1];
            String apellidoP = null;
            String apellidoM = null;
            int tokenNamesLength = nameTokens.length;
            if (tokenNamesLength == 4) {
                apellidoP = nameTokens[1];
                apellidoM = nameTokens[2];
            } else if (tokenNamesLength > 2) {
                apellidoP = nameTokens[0];
                apellidoM = nameTokens[1];
            } else if (tokenNamesLength == 2) {
                apellidoP = nameTokens[0];
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("nameFull", namePreParsed);
            jsonObject.put("name", name);
            jsonObject.put("apellidoP", apellidoP);
            jsonObject.put("apellidoM", apellidoM);
            return jsonObject;
        } catch (Exception e) {
            log.error("Could not extract name fallback: {} with content: {}", e.getMessage(), content);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("nameFull", "NA");
            jsonObject.put("name", "NA");
            jsonObject.put("apellidoP", "NA");
            jsonObject.put("apellidoM", "NA");
            return jsonObject;
        }
    }

    private JSONObject parseName(String content) {
        content = content.toUpperCase();
        try {
            String token1Name = "NOMBRE";
            String tokenName2 = "DOMICILIO";
            String namePreParsed = parseInfo(content, token1Name, tokenName2);
            namePreParsed = namePreParsed.replace("FECHA DE NACIMIENTO", "");
            namePreParsed.replaceAll("\\\\n\\d\\d\\/\\d\\d\\/\\d\\d\\d\\d", "");
            namePreParsed.replaceAll("\\d\\d\\/\\d\\d\\/\\d\\d\\d\\d", "");
            namePreParsed = namePreParsed.substring(6);
            if (namePreParsed.startsWith("\\n")) {
                namePreParsed = namePreParsed.substring(2);
            }
            String[] nameTokens = namePreParsed.split("\\n");
            String name = nameTokens[nameTokens.length - 1];
            String apellidoP = null;
            String apellidoM = null;
            int tokenNamesLength = nameTokens.length;
            if (tokenNamesLength == 4) {
                apellidoP = nameTokens[1];
                apellidoM = nameTokens[2];
            } else if (tokenNamesLength > 2) {
                apellidoP = nameTokens[0];
                apellidoM = nameTokens[1];
            } else if (tokenNamesLength == 2) {
                apellidoP = nameTokens[0];
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("nameFull", namePreParsed);
            jsonObject.put("name", name);
            jsonObject.put("apellidoP", apellidoP);
            jsonObject.put("apellidoM", apellidoM);
            return jsonObject;
        } catch (Exception e) {
            log.error("Could not extract 'Name' , going to fallback: ", e.getMessage());
            return null;
        }
    }


    private String parseInfo(String raw, String token1, String token2) {
        int place1 = raw.indexOf(token1);
        int place2 = raw.indexOf(token2);
        raw = raw.substring(place1, place2);
        if (raw.startsWith("\\n")) {
            raw = raw.substring(2);
        }
        return raw;
    }

}
