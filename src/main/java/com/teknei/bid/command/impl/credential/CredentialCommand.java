package com.teknei.bid.command.impl.credential;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.repository.*;
import com.teknei.bid.util.icar.IcarDataSingleton;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.UUID;

@Component
public class CredentialCommand implements Command {

    @Autowired
    @Qualifier(value = "parseCredentialVisionCommand")
    private Command parseCredentialCommand;
    @Autowired
    @Qualifier(value = "parseCredentialProxyCommand")
    private Command parseCredentialCommandProxy;
    @Autowired
    @Qualifier(value = "parseCredentialCommand")
    private Command parseCredentialCommandIcar;
    @Autowired
    @Qualifier(value = "persistCredentialCommand")
    private Command persistCredentialCommand;
    @Autowired
    @Qualifier(value = "storeTasCredentialCommand")
    private Command storeTasCredentialCommand;
    @Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;
    @Autowired
    @Qualifier(value = "registerRecordCommand")
    private Command registerRecordCommand;
    @Autowired
    @Qualifier(value = "parseFakeCredentialCommand")
    private Command parseFakeCredentialCommand;
    @Autowired
    @Qualifier(value = "parseCredentialNapiCommand")
    private Command parseNapiCredentialCommand;
    @Autowired
    private BidClieRegEstaRepository regEstaRepository;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Autowired
    private BidClieRepository bidClieRepository;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    @Value("${tkn.identification.ocr.active}")
    private Boolean activeOcr;
    @Value("${tkn.identification.tas.active}")
    private Boolean activeTas;
    @Value("${tkn.identification.persist.active}")
    private Boolean activePersistence;
    @Value("${tkn.identification.ocr.provider}")
    private String ocrProvider;
    private static final String ESTA_PROC = "CAP-CRE";
    private static final Logger log = LoggerFactory.getLogger(CredentialCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
        String documentId = "";
        String scanId = "";
        CommandResponse response = null;
        if(request.getId() == null || request.getId() == 0){
            if (ocrProvider.toUpperCase().contains("ICAR")) {
                response = parseCredentialCommandIcar.execute(request);
            } else if(ocrProvider.toUpperCase().contains("PROXY")){
                response = parseCredentialCommandProxy.execute(request);
            }else if(ocrProvider.toUpperCase().contains("FAKE")){
                response = parseFakeCredentialCommand.execute(request);
            }else if(ocrProvider.toUpperCase().contains("NAPI")){
                response = parseNapiCredentialCommand.execute(request);
            }else{
                response = parseCredentialCommand.execute(request);
            }
            response.setStatus(Status.CREDENTIALS_OK);
            response.setDesc(response.getDesc());
            response.setScanId(scanId);
            response.setDocumentId(documentId);
            return response;
        }
        registerRecordCommand.execute(request);
        if (activeOcr) {
            if (ocrProvider.toUpperCase().contains("ICAR")) {
                response = parseCredentialCommandIcar.execute(request);
            } else if(ocrProvider.toUpperCase().contains("PROXY")){
                response = parseCredentialCommandProxy.execute(request);
            }else if(ocrProvider.toUpperCase().contains("FAKE")){
                response = parseFakeCredentialCommand.execute(request);
            }else if(ocrProvider.toUpperCase().contains("NAPI")){
                response = parseNapiCredentialCommand.execute(request);
            }else{
                response = parseCredentialCommand.execute(request);
            }
            if (!response.getStatus().equals(Status.CREDENTIALS_ICAR_OK)) {
                saveStatus(request.getId(), Status.CREDENTIALS_ICAR_ERROR, request.getUsername());
                return response;
            }
            saveStatus(request.getId(), Status.CREDENTIALS_ICAR_OK, request.getUsername());
        } else {
            scanId = createDummyScanId(request.getId(), request.getUsername());
            IcarDataSingleton.getInstance().getImageMap().put(scanId + "_front", request.getFileContent().get(0));
            if (request.getFileContent().size() > 1) {
                IcarDataSingleton.getInstance().getImageMap().put(scanId + "_back", request.getFileContent().get(1));
            }
            saveStatus(request.getId(), Status.CREDENTIALS_ICAR_OK, request.getUsername());
            response = new CommandResponse();
            //Build compatible JSON answer
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("scanId", scanId);
            jsonObject.put("code", "OK");
            JSONObject document = new JSONObject();
            document.put("CRC_SECTION", "NA");
            document.put("MRZ", "NA");
            document.put("MRZ", "NA");
            BidClie bidClie = bidClieRepository.findOne(request.getId());
            document.put("surname", bidClie.getApePate() + " " + bidClie.getApeMate());
            document.put("firstSurname", bidClie.getApePate());
            document.put("secondSurname", bidClie.getApeMate());
            document.put("name", bidClie.getNomClie());
            document.put("address", "NA");
            document.put("curp", "NA");
            document.put("dateOfBirth", "NA");
            jsonObject.put("document", document);
            response.setDesc(jsonObject.toString());
            response.setId(request.getId());
        }
        if (activeTas) {
            CommandRequest tasRequest = new CommandRequest();
            tasRequest.setData(bidTasRepository.findByIdClie(request.getId()).getIdTas());
            tasRequest.setId(request.getId());
            tasRequest.setScanId(scanId);
            tasRequest.setFileContent(request.getFileContent());
            CommandResponse tasResponse = storeTasCredentialCommand.execute(tasRequest);
            if (!tasResponse.getStatus().equals(Status.CREDENTIALS_TAS_OK)) {
                saveStatus(request.getId(), Status.CREDENTIALS_TAS_ERROR, request.getUsername());
                return tasResponse;
            }
            documentId = tasResponse.getDocumentId();
            response.setDocumentId(documentId);
            scanId = tasRequest.getScanId();
            saveStatus(request.getId(), Status.CREDENTIALS_TAS_OK, request.getUsername());
        } else {
            saveStatus(request.getId(), Status.CREDENTIALS_TAS_OK, request.getUsername());
        }
        if (activePersistence) {
            CommandRequest dbRequest = new CommandRequest();
            dbRequest.setId(request.getId());
            dbRequest.setData(response.getDesc());
            dbRequest.setDocumentId(documentId);
            dbRequest.setScanId(response.getScanId());
            dbRequest.setUsername(request.getUsername());
            CommandResponse dbResponse = persistCredentialCommand.execute(dbRequest);
            if (dbResponse.getStatus().equals(Status.CREDENTIALS_DB_OK)) {
                saveStatus(request.getId(), Status.CREDENTIALS_DB_OK, request.getUsername());
                saveStatus(request.getId(), Status.CREDENTIALS_OK, request.getUsername());
                response.setStatus(Status.CREDENTIALS_OK);
                response.setDesc(response.getDesc());
                response.setScanId(scanId);
                response.setDocumentId(documentId);
                //updateStatus(request.getId(), request.getUsername());
            } else {
                saveStatus(request.getId(), Status.CREDENTIALS_DB_ERROR, request.getUsername());
            }
        } else {
            saveStatus(request.getId(), Status.CREDENTIALS_DB_OK, request.getUsername());
        }
        return response;
    }


    private String createDummyScanId(Long idOperation, String username) {
        BidScan bidScan = bidScanRepository.findByIdRegi(idOperation);
        if (bidScan == null) {
            String scanId = UUID.randomUUID().toString();
            bidScan = new BidScan();
            bidScan.setUsrCrea(username);
            bidScan.setFchCrea(new Timestamp(System.currentTimeMillis()));
            bidScan.setIdRegi(idOperation);
            bidScan.setScanId(scanId);
            bidScan.setData("");
            bidScan.setUsrOpeCrea(username);
            bidScanRepository.save(bidScan);
            return scanId;
        }
        return bidScan.getScanId();
    }

    /**
     * Persists the current status for the main process
     *
     * @param id
     * @param status
     * @return
     */
    private CommandResponse saveStatus(Long id, Status status, String username) {
        CommandRequest request = new CommandRequest();
        request.setUsername(username);
        request.setId(id);
        request.setRequestStatus(status);
        CommandResponse response = statusCommand.execute(request);
        return response;
    }

    private void updateStatus(Long idClient, String username) {
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(ESTA_PROC, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
}
