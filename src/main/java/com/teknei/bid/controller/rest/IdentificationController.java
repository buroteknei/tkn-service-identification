package com.teknei.bid.controller.rest;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.*;
import com.teknei.bid.persistence.entities.BidClieIfeIne;
import com.teknei.bid.persistence.entities.BidClieIfeInePK;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidIfeRepository;
import com.teknei.bid.service.CurpService;
import com.teknei.bid.service.IdentificationService;
import com.teknei.bid.service.remote.ByPassCaller;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/identification")
public class IdentificationController {

    @Autowired
    @Qualifier(value = "credentialCommand")
    private Command credentialCommand;
    @Autowired
    @Qualifier(value = "additionalCredentialCommand")
    private Command additionalCredentialsCommand;
    @Autowired
    @Qualifier(value = "captureCommand")
    private Command captureCommand;
    @Autowired
    private IdentificationService identificationService;
    @Autowired
    private BidIfeRepository bidIfeRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidClieRegEstaRepository regEstaRepository;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    @Autowired
    private CurpService curpService;
    private static final String ESTA_PROC = "VAL-SERV-INE";
    private static final String ESTA_PROC_2 = "RES-INE";
    private static final Logger log = LoggerFactory.getLogger(IdentificationController.class);

    @ApiOperation(value = "Verifies the information against INE CECOBAN stage", response = String.class)
    @RequestMapping(value = "/verify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> verifyAgainstINE(@RequestBody PersonDataIneTKNRequest personData) {
        try {
            updateStatus(personData.getId(), ESTA_PROC);
            JSONObject jsonObject = identificationService.verifyCecoban(personData);
            //TODO save request? save response from remote servera
            updateStatus(personData.getId(), ESTA_PROC_2);
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error for make request to cecoban with message: {}", e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
     
    @ApiOperation(value = "Finds the detail parsed for a particular parsed address", response = IneDetailDTO.class)
    @RequestMapping(value = "/findDetail/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<IneDetailDTO> findDetail(@PathVariable Long id) {
        IneDetailDTO dto = new IneDetailDTO();
        try {
            BidClieIfeInePK pk = new BidClieIfeInePK();
            pk.setIdClie(id);
            pk.setIdIfe(id);
            BidClieIfeIne retrieved = bidIfeRepository.findOne(pk);
            if (retrieved == null) {
                return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
            }
            dto.setApeMat(retrieved.getApeMate());
            dto.setApePat(retrieved.getApePate());
            dto.setCall(retrieved.getCall());
            dto.setClavElec(retrieved.getClavElec());
            dto.setCol(retrieved.getCol());
            dto.setCp(retrieved.getCp());
            dto.setDist(retrieved.getDist());
            dto.setEsta(retrieved.getEsta());
            dto.setFoli(retrieved.getFoli());
            dto.setLoca(retrieved.getLoca());
            dto.setMrz(retrieved.getMrz());
            dto.setMuni(retrieved.getMuni());
            dto.setNoExt(retrieved.getNoExt());
            dto.setNoInt(retrieved.getNoInt());
            dto.setOcr(retrieved.getOcr());
            dto.setNomb(retrieved.getNomb());
            dto.setSecc(retrieved.getSecc());
            if (retrieved.getVige() != null) {
                dto.setVige(retrieved.getVige().getTime());
            }
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding detail for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Updates manually data retrieved by parsing systems", notes = "The 'user' field represents the user logged into system that is actually making the update", response = String.class)
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> updateCredentialData(@PathVariable Long id, @RequestBody IneDetailDTO detailDTO) {
        if (detailDTO.getCurp() != null && !detailDTO.getCurp().isEmpty()) {
            try {
                bidCurpRepository.updateCurp(detailDTO.getCurp(), id);
            } catch (Exception e) {
                log.error("Error updating base curp for proces with message: {}", e.getMessage());
            }
        }
        try {
            BidClieIfeInePK pk = new BidClieIfeInePK();
            pk.setIdClie(id);
            pk.setIdIfe(id);
            BidClieIfeIne retrieved = bidIfeRepository.findOne(pk);
            retrieved.setNomb(detailDTO.getNomb());
            retrieved.setApePate(detailDTO.getApePat());
            retrieved.setApeMate(detailDTO.getApeMat());
            retrieved.setCall(detailDTO.getCall());
            retrieved.setNoExt(detailDTO.getNoExt());
            retrieved.setNoInt(detailDTO.getNoInt());
            retrieved.setCol(detailDTO.getCol());
            retrieved.setCp(detailDTO.getCp());
            retrieved.setFoli(detailDTO.getFoli());
            retrieved.setClavElec(detailDTO.getClavElec());
            retrieved.setOcr(detailDTO.getOcr());
            retrieved.setEsta(detailDTO.getEsta());
            retrieved.setMuni(detailDTO.getMuni());
            retrieved.setLoca(detailDTO.getLoca());
            retrieved.setDist(detailDTO.getDist());
            retrieved.setSecc(detailDTO.getSecc());
            retrieved.setVige(new Timestamp(detailDTO.getVige()));
            retrieved.setMrz(detailDTO.getMrz());
            retrieved.setFchModi(new Timestamp(System.currentTimeMillis()));
            retrieved.setUsrModi(detailDTO.getUser());
            retrieved.setCurp(detailDTO.getCurp());
            retrieved.setUsrOpeModi(detailDTO.getUser());
            bidIfeRepository.save(retrieved);
            return new ResponseEntity<>("Successful update", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error updating manually address {} for id : {} with message: {}", id, detailDTO, e.getMessage());
            return new ResponseEntity<>("Error updating detail", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Uploads file(s) related to the additional identification of the customer, parses it and stores in the casefile", notes = "The attachment(s) must be named 'file'. The first identification must be front, the second (optional) must be back", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the process is correct"),
            @ApiResponse(code = 422, message = "If the identification could not be recognized")
    })
    @RequestMapping(value = "/uploadAdditional/{id}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> uploadAdditionalCredentials(@RequestPart(value = "file") List<MultipartFile> files, @PathVariable Long id) {
        return upload(files, id, additionalCredentialsCommand);
    }


    @ApiOperation(value = "Uploads file(s) related to the additional identification in plain of the customer, parses it and stores in the casefile", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the process is correct"),
            @ApiResponse(code = 422, message = "If the identification could not be recognized")
    })
    @RequestMapping(value = "/uploadAdditionalPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> uploadAdditionalCredentials(@RequestBody RequestEncFilesDTO dto) {
        return processEncFiles(dto, additionalCredentialsCommand);
    }


    @ApiOperation(value = "Uploads capture screen related to the additional identification of the customer, parses it and stores in the casefile", notes = "The attachment(s) must be named 'file", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the process is correct"),
            @ApiResponse(code = 422, message = "If the capture could not be recognized")
    })
    @RequestMapping(value = "/uploadCapture/{id}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> uploadCaptureCredentials(@RequestPart(value = "file") List<MultipartFile> files, @PathVariable Long id) {
        return upload(files, id, captureCommand);
    }


    @ApiOperation(value = "Uploads capture screen in plain related to the additional identification of the customer, parses it and stores in the casefile", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the process is correct"),
            @ApiResponse(code = 422, message = "If the capture could not be recognized")
    })
    @RequestMapping(value = "/uploadCapturePlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> uploadCaptureCredentials(@RequestBody RequestEncFilesDTO dto) {
        return processEncFiles(dto, captureCommand);
    }

    @ApiOperation(value = "Uploads file(s) related to the identification of the customer, parses it and stores in the casefile", notes = "The attachment(s) must be named 'file'", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the process is correct"),
            @ApiResponse(code = 422, message = "If the identification could not be recognized")
    })
    @RequestMapping(value = "/upload/{id}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> uploadCredentials(@RequestPart(value = "file") List<MultipartFile> files, @PathVariable Long id) {
        return upload(files, id, credentialCommand);
    }

    @ApiOperation(value = "Uploads file(s) related to the identification of the customer, parses it and stores in the casefile", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the process is correct"),
            @ApiResponse(code = 422, message = "If the identification could not be recognized")
    })
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> uploadCredentialsAsync(@RequestBody RequestEncFilesDTO dto) {
        return processEncFiles(dto, credentialCommand);
    }


    @ApiOperation(value = "Downloads the resource (identification) related to the customer", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the picture is found"),
            @ApiResponse(code = 404, message = "If the picture is nof found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto) {
        byte[] image = null;
        try {
            image = identificationService.findPicture(dto);
            if (image == null) {
                return new ResponseEntity<>(image, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(image, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding image for reference: {} with message: {}", dto, e.getMessage());
            return new ResponseEntity<>(image, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/curp/obtain", method = RequestMethod.POST)
    public ResponseEntity<String> getCurp(@RequestBody CurpRequestDTO curpRequestDTO) {
        String response = null;
        switch (curpRequestDTO.getType()) {
            case 1:
                response = curpService.getCurp(curpRequestDTO.getRequestByCustomerData().getIdCustomer(), curpRequestDTO.getRequestByCustomerData().getEntidad(), curpRequestDTO.getDocument());
                break;
            case 2:
                response = curpService.getCurp(curpRequestDTO.getRequestByExternalData().getName(), curpRequestDTO.getRequestByExternalData().getSurname(), curpRequestDTO.getRequestByExternalData().getSurnameLast(), curpRequestDTO.getRequestByExternalData().getBirthDate(), curpRequestDTO.getRequestByExternalData().getEntidad(), curpRequestDTO.getRequestByExternalData().getGender(), curpRequestDTO.getDocument());
                break;
            default:
                response = null;
                break;
        }
        if (response == null) {
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/curp/validate", method = RequestMethod.POST)
    public ResponseEntity<String> validateCurp(@RequestBody CurpRequestDTO curpRequestDTO) {
        String response = null;
        switch (curpRequestDTO.getType()) {
            case 3:
                response = curpService.validateCurp(curpRequestDTO.getValidateByCustomerData().getIdCustomer(), curpRequestDTO.getDocument());
                break;
            case 4:
                response = curpService.validateCurp(curpRequestDTO.getValidateByExternalData().getCurp(), curpRequestDTO.getDocument());
                break;
            default:
                response = null;
                break;
        }
        if (response == null) {
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Rollback the status for primary id captured")
    @RequestMapping(value = "/rollback/identificationCapture", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> rollbackConfirmationCredentialCapture(@RequestBody BasicRequestDTO basicRequestDTO){
        List<String> listToRollback = Arrays.asList(new String[]{"CAP-CRE", "CAP-VALINE"});
        listToRollback.forEach(r -> rollbackStatus(basicRequestDTO.getIdOperation(), r, basicRequestDTO.getUsername()));
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    private ResponseEntity<String> upload(List<MultipartFile> files, Long id, Command processingCommand) {
        CommandRequest request = new CommandRequest();
        request.setFiles(files);
        request.setId(id);
        request.setUsername("tkn-api");
        CommandResponse response = processingCommand.execute(request);
        if (response.getStatus().equals(Status.CREDENTIALS_OK)) {
            return new ResponseEntity<>(response.getDesc(), HttpStatus.OK);
        }
        return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
    }
     
    private void updateStatus(Long idClient, String status) {
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi("client-api");
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }

    private void rollbackStatus(Long idClient, String status, String username) {
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(false);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }

    private ResponseEntity<String> processEncFiles(RequestEncFilesDTO dto, Command processingCommand) {
        CommandRequest request = new CommandRequest();
        request.setUsername(dto.getUsername());
        ArrayList<byte[]> contentFiles = new ArrayList<>();
        byte[] content1 = Base64Utils.decodeFromString(dto.getB64Anverse());
        contentFiles.add(content1);
        byte[] content2 = null;
        if (dto.getB64Reverse() != null && !dto.getB64Reverse().isEmpty()) {
            content2 = Base64Utils.decodeFromString(dto.getB64Reverse());
            contentFiles.add(content2);
        }
        request.setFileContent(contentFiles);
        request.setId(dto.getOperationId());
        CommandResponse response = processingCommand.execute(request);
        if (response.getStatus().equals(Status.CREDENTIALS_OK)) {
            return new ResponseEntity<>(response.getDesc(), HttpStatus.OK);
        }
        return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
    }

}