package com.teknei.bid;

import com.teknei.bid.controller.rest.IdentificationController;
import com.teknei.bid.dto.IneDetailDTO;
import com.teknei.bid.persistence.repository.BidIfeRepository;
import com.teknei.bid.service.CurpService;
import com.teknei.bid.util.cecoban.CecobanUtil;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TknServiceIdentificationApplicationTests {

    private static final Logger log = LoggerFactory.getLogger(TknServiceIdentificationApplicationTests.class);
    private static CharsetEncoder asciiEncoder =
            Charset.forName("ISO-8859-1").newEncoder();
   // @Autowired
    private IdentificationController identificationController;
    //@Autowired
    private BidIfeRepository bidIfeRepository;
    //Properties
    //@Autowired
    private CecobanUtil cecobanUtil;
    //@Autowired
    private TasManager tasManager;
    //@Autowired
    private CurpService curpService;

    private String[] pasaportes = {"\"text\": \"*GN sod\\nSlik Es\\nSTADOS U\\nLa Secretaria de Relaciones Exteriore\\nMexicanos solicita a las autoridad-\\npermitan al titular de este pasaported\\nSu libre paso sin retraso u obstáculo :\\notorguen toda la asistencia y p\\naid and protect\\nThe Ministry of Foreign Affairs of the\\nhereby requests all competent authorit\\nthis passport, a Mexican national, fre\\nhindrance and in case of need it\\nLe Ministère des Affaires Etrangeres d\\nprie les autorites competentes de hi\\nlibrement et sans entrave le titulaire\\nnationalite mexicaine, et de lui prerer ou\\nPASAPORTE Ptil Estados Unidos Mexicanos\\nClave del país de expedición\\nPasaporte No.\\nTipo\\nPaspon No. / G17322489\\nCathporne Cade du pays émetteur\\nIssuing state codel MIEX\\nio. du Passeport\\nApellidos /Surname/ Nam\\nSALINAS ARTEAGA\\nNombres / Given nama Prénoms\\nALEJANDRO\\nNacionalidad Nationality/ Nationalità\\nvaciones / Romerks Observations\\nMIEXICANA\\nFecha de nacimiento pate of birth Dato de naissance\\nPat No / No personnel\\n18 11 1986\\nSexo Sed Sexe Lugar de nacimiento place of birth\\nDISTRITO FEDERAL\\n19167776\\nFecha de expedicion / Assun datow Date de délivrance Autoridad Authority Autorité\\n12 05 2015\\nFecha de caducidad /Expiry date/ Dato d'expiration\\n12 05 2018\\nFirma del titular\\nNAUCALPAN\\nHalders signature Signature du titulaire\\nP&lt;MEX SALINAS &lt; ARTEAGA&lt;&lt;ALEJANDRO&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;\\nG173224892MEX8611183M1805121&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;08\\n\"", "\"text\": \"UNIDOS M\\nod put pe\\nVn sogy\\nla Secretario de Relaciones Exteri\\nMexicanos solicita a las autori\\npermitan al titular de este pasapor\\nsu libre paso sin retrasou obstácu\\notorguen toda la asistencia\\nThe Ministry of Foreign Affairs\\nhereby requests all competent aut\\nthis passport, a Mexican national\\nhindrance and in case of ne\\nLe Ministère des Affaires Etrange\\npre les autorites compétentes\\nlibrement et sans entrave letits\\nnationalité mexicainc, et de lui préte\\n- P code du paya nella\\nPASAPORTEH Estados Unidos Mexicanos\\nClave del país de expedición\\nPasaporte No.\\nTipo\\nPerior G17322489\\nMEX\\nApellidos i Surname/Nomi\\nSALINAS ARTEAGA\\nNombres. Given names\\nALEJANDRO\\nQuervaciones /Ramers Observatoru\\nNacionalidad Nationally National\\nMEXICANA\\nFecha de nacimiento pan albamu bas de canca\\nNa/No penone\\n18 11 1986\\nSexo (se So Lugar de nacimiento DISTRITO FEDERAL\\nFecha de expedición lacun dator al de drama\\nAutoridad Authority Auto\\n12 052015\\nFecha de caducidadEpiry dan Cele doupation\\n12 05 2018\\nNAUCALPAN\\nFirma del titular\\nHoldur signature Signature du fain\\nP&lt;MEX SALINAS&lt;ARTEAGA&lt;&lt;ALEJANDRO&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;\\nG173224892MEX8611183M1805 121&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;08\\n\"", "\n" +
            "    \"text\": \"Pasaporte No.\\nPASA PORTER Estados Unidos Mexicanos\\nTipo Clave del país de expedición\\nG17322489\\nPoport\\nMEX\\nApellidos /m/ Non\\nSALINAS ARTEAGA\\nNombres /Grename here\\nALEJANDRO\\nQUservaciones pamatu Observations\\nNacionalidad ay na\\nMEXICANA\\nFecha de nacimiento Diev Da descURPINO/ No personnel\\n18 11 1986\\nSexo Sugar de nacimiento de\\nDISTRITO FEDERAL\\nFecha de expedicion de\\nna Autoridad Authority Aute\\n12 05 2015\\nFecha de caducidad/phy v betegn\\n12 05 2018\\nFirma del titular\\nP&lt;MEX SALINAS &lt;ARTEA GA&lt;&lt;ALEJANDRO &lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;\\nG173224892MEX8611183M 1805121&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;08\\n\"\n" +
            "\n",
            "    \"text\": \"Pasaporte No.\\nPASA PORTER Estados Unidos Mexicanos\\nTipo Clave del país de expedición\\nG17322489\\nPoport\\nMEX\\nApellidos /m/ Non\\nSALINAS ARTEAGA\\nNombres /Grename here\\nALEJANDRO\\nQUservaciones pamatu Observations\\nNacionalidad ay na\\nMEXICANA\\nFecha de nacimiento Diev Da descURPINO/ No personnel\\n18 11 1986\\nSexo Sugar de nacimiento de\\nDISTRITO FEDERAL\\nFecha de expedicion de\\nna Autoridad Authority Aute\\n12 05 2015\\nFecha de caducidad/phy v betegn\\n12 05 2018\\nFirma del titular\\nP&lt;MEX SALINAS &lt;ARTEA GA&lt;&lt;ALEJANDRO &lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;\\nG173224892MEX8611183M 1805121&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;08\\n\"\n",
            "\"text\": \"ICAN\\nE TUAE tentera\\nla Secretaria de Relaciones Exteriores de los Estados Unidos\\nMexicanos solicita a las autoridades competentes que\\npermitan al titular de este pasaporte de nacionalidad mexicana\\nSa lihreasa: sin retraso u obstáculo alguno y dado elezso de\\nSKILGISCdonomƏgd i EIDUISTSE EI Ppca Badanio\\nTIR Mnistry of Foreign Affairs of the United MLNican States\\nhereby requests al competenu authorities permit tire holder af\\n10 Meap anonim SHELL SI penonBG UROIXON E AJUissri sty1\\nfinalTE Iunu DAIS 01 poƏu 10 ase), tap pires lieupptary\\nPuo11991oodpun pue\\nJe Ministere des Affaires Etrangères des États-Unis du Mexique\\nprie les autorités competentes de bien vouloir laisser passer\\nlibrement et sans entrave le titulaire du persent passeport, de\\nnationalité mexicaine, et de lui préter toute aide er assistance possibiles\\nSTADOS\\nPASAPORTE Pas opera Estados Unidos Mexicanos\\nClave del país de expedicion\\nPasaporte No.\\nТура\\nIssuing stato coda\\nCatégorieCode pays ernetteur\\nPassporn No G02542431\\nMEX\\n10. du Passeport\\nApellidos / Sumamo Nom\\nBAÑUELOS LOPEZ\\nNombres Givors nomes Prénoms\\nANDRES\\nNacionalidad / Nationality/ Natioevalitdi\\nObservaciones Romarksi Observatons\\nMEXICANA\\nFecha de nacimiento / Date of birth/ Date de naissanceGLIRP / Pers(Gal No No personnel\\n12 03 1968\\nSexo /So Sexe Lugar de nacimiento Placant birthet jaar dan nama ptice in\\nMEXICO, D.F\\nFecha de expedición Issue date Dame de delivrance\\nAutoridad Paiority Aulotila\\n22 04 2009\\nFecha de caducidadi Expiry datal Date d'expiration\\n22 042019\\nCUAUHTEMOC\\nFirma del titular\\nIN LISB2TEKAKE\\nHolder's signatural Signature di Britaire\\np&lt;MEXBANUE LOS&lt;LOPEZ&lt;&lt; ANDRES &lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;\\nG0254 243 11 MEX6803122 M1904 220 &lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;02\\n\""};

    /*@Test
    public void testCurp() {
        String answer = curpService.getCurp(16l, "MC");
        log.info("Answer: {}", answer);
        String answer2 = curpService.validateCurp("AACJ881203HMCMRR08");
        log.info("Answer2: {}", answer2);
    }
*/

    //@Test
    public void testPasaporte() {
        for (String s :
                pasaportes) {
            getPassportNumber(s);
            getPassportType(s);
            getPassportExpeditionCountryCode(s);
            getSurnames(s);
            getNames(s);
            getBirth(s);
            getGender(s);
            getBirthPlace(s);
            getDateExp(s);
            getDateCaducity(s);
        }
    }

    private String getDateCaducity(String text) {
        String regex = "\\d{2}\\s\\d{2}\\s\\d{4}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        List<String> matches = new ArrayList<>();
        while (matcher.find()) {
            String content = text.substring(matcher.start(), matcher.end());
            matches.add(content);
        }
        if (matches.isEmpty()) {
            return "01 01 1970";
        }
        if (matches.size() >= 3) {
            log.info("Found: {}", matches.get(2));
            return matches.get(2);
        }
        return "01 01 1970";
    }

    private String getDateExp(String text) {
        String regex = "\\d{2}\\s\\d{2}\\s\\d{4}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        List<String> matches = new ArrayList<>();
        while (matcher.find()) {
            String content = text.substring(matcher.start(), matcher.end());
            matches.add(content);
        }
        if (matches.isEmpty()) {
            return "01 01 1970";
        }
        if (matches.size() >= 2) {
            log.info("Found: {}", matches.get(1));
            return matches.get(1);
        }
        return "01 01 1970";
    }

    private String getBirthPlace(String text) {
        String token = "Fecha de ex";
        if (text.contains(token)) {
            text = text.substring(text.indexOf(token) - 50, text.indexOf(token));
            String regex = "[A-Z]{3,15}(,\\s...|(\\s[A-Z]{3,15}|\\s))";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                String content = text.substring(matcher.start(), matcher.end());
                log.info("Place of birth: {}", content);
                return content;
            }
            return "NO RECONOCIDO";
        }
        return "NO RECONOCIDO";
    }

    private String getGender(String text) {
        return "M";
    }

    private String getBirth(String text) {
        String regex = "\\d{2}\\s\\d{2}\\s\\d{4}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String precontent = text.substring(matcher.start(), matcher.end());
            precontent = precontent.replace("\\\\n", "").replace("\\n", "").trim();
            log.info("Precontent1: {}", precontent);
            return precontent;
        }
        return "01 01 1970";
    }

    private String getNames(String text) {
        String regex = "\\\\n[A-ZÑ]{4,20}\\\\n";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String precontent = text.substring(matcher.start(), matcher.end());
            precontent = precontent.replace("\\\\n", "").replace("\\n", "").trim();
            log.info("Precontent1: {}", precontent);
            return precontent;
        }
        return "NO LEGIBLE";
    }

    private String getSurnames(String text) {
        text = text.substring(50);
        String regex = "[A-ZÑ]{3,20}\\s[A-ZÑ]{3,20}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String precontent = text.substring(matcher.start(), matcher.end());
            log.info("Precontent1: {}", precontent);
            return precontent;
        }
        return "NO LEGIBLE";
    }

    private String getPassportExpeditionCountryCode(String text) {
        String regex = "\\\\n[A-Z]{3,5}\\\\n";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String precontent = text.substring(matcher.start(), matcher.end());
            precontent = precontent.replace("\\\\n", "").replace("\\n", "").trim();
            log.info("Country expedition found: {}", precontent);
            return precontent;
        }
        return "MEX";
    }

    private String getPassportNumber(String text) {
        text = text.replace(" ", "");
        String regex = "\\w{4,20}MEX\\d{3}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String preContent = text.substring(matcher.start(), matcher.end());
            if (preContent.startsWith("n")) {
                preContent = preContent.substring(1, preContent.length());
            } else {
                int index = preContent.indexOf("\\n");
                if (index != -1) {
                    preContent = preContent.substring(index, preContent.length());
                }
            }
            preContent = preContent.trim();
            log.info("Precontent: {}", preContent);
            return preContent;
        }
        return "TESTMX002";
    }

    private String getPassportType(String text) {
        String regex = "\\\\n[a-zA-Z]&lt;[A-Z]{2,3}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String type = text.substring(matcher.start(), matcher.end());
            type = type.substring(2, 3);
            log.info("Type found:{}", type);
            return type;
        }
        return "P";
    }

    //@Test
    public void testCapture() throws IOException {
        byte[] content = Files.readAllBytes(Paths.get("/home/amaro/Descargas/ALEJANDRO_ESCOBAR.pdf"));
        MockMultipartFile mpf = new MockMultipartFile("PDF_TEST", content);
        identificationController.uploadCaptureCredentials(Arrays.asList(mpf), 46l);
    }

    //@Test
    public void testAdditionalCapture() throws IOException {
        byte[] content1 = Files.readAllBytes(Paths.get("/home/amaro/Descargas/62.pdf"));
        byte[] content2 = Files.readAllBytes(Paths.get("/home/amaro/Descargas/contrato2.pdf"));
        MockMultipartFile mpf = new MockMultipartFile("First", content1);
        MockMultipartFile mpf2 = new MockMultipartFile("Second", content2);
        identificationController.uploadAdditionalCredentials(Arrays.asList(mpf, mpf2), 46l);
    }

    //@Test
    public void testUpdateTas() {
        //  tasManager.updateDocument("000058d1105727cb4a12a9de9677e3081210");
    }

    @Test
    public void testClaveElector() throws JSONException {
        String content = "INSTITUTO FEDERAL ELECTORAL\n" +
                "REGISTRO FEDERAL DE ELECTORES\n" +
                "CREDENCIAL PARA VOTAR\n" +
                "NOMBRE\n" +
                "EDAD 31\n" +
                "SEXO M\n" +
                "OJEDA\n" +
                "REYES\n" +
                "SILVIA DULCINEA\n" +
                "DOMICILIC\n" +
                "AV FRANCISCO J MACIN 147\n" +
                "U CTM EL RISCO 07090\n" +
                "GUSTAVO A. MADERO ,D.F.\n" +
                "FOIO 0000101063859 AÑO DE REGISTRO 199h 01\n" +
                "CLAVE DE ELECTOR OJRYSI 79022609M600\n" +
                "CURP OERS790226MDFJY0\n" +
                "ESTADO 09 MUNICIP\n" +
                "LOCALIDAD 0001 SECCN\n" +
                "EMISION 2010 VIGNC\n" +
                "20\n" +
                "FIRMA\n";
        log.info("Address parsed: {}", parseAddresIfe(content));
        log.info("Clave elector parsed: {}", parseClaveElector(content));
    }

    private String parseClaveElector(String content) {
        String fallback = content;
        try {
            String token1Name = "CLAVE DE ELECTOR";
            int index = content.indexOf(token1Name);
            content = content.substring(index);
            int end = content.indexOf("\n");
            content = content.substring(16, end);
            content = content.replace("\n", "");
            content = content.trim();
            if (asciiEncoder.canEncode(content)) {
                return content;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("Could not extract 'ClaveElector' : {} going fallback", e.getMessage());
            return parseClaveElectorFallback(fallback);
        }
    }

    private String parseAddresIfe(String content) {
        try {
            String token1Name = "DOMICILIO";
            String token1Name1 = "DOMILIC";
            String token1Name2 = "DOMICILICO";
            String token1Name3 = "DOMICILIC";
            String token1Name4 = "ICILI";
            String token1Name5 = "DOMICILI0";
            String token1Name6 = "OOMICILIO";
            String[] tokens1 = {token1Name, token1Name1, token1Name2, token1Name3, token1Name6, token1Name4, token1Name5};
            String[] tokenEnds = {"CLAVE DE ELECTOR", "ELECTOR", "LECTOR", "FOLIO", "FOLI0", "F0LIO", "F0LI0", "AÑO DE REGISTRO", "ANO DE REGISTRO", "ECHA DE NACRENTO", "FECHA DE NACIMIENTO", "FOIO"};
            String parsed = null;
            for (String s : tokens1) {
                if (content.contains(s)) {
                    for (String end : tokenEnds) {
                        if (content.contains(end)) {
                            int tIndex = content.indexOf(end);
                            if (content.indexOf(s) < tIndex) {
                                parsed = parseInfo(content, s, end);
                            } else {
                                int tIndex2 = content.lastIndexOf(end);
                                if (content.indexOf(s) < tIndex2) {
                                    parsed = content.substring(content.indexOf(s), content.lastIndexOf(end));
                                } else {
                                    continue;
                                }
                            }
                            for (String se : tokenEnds) {
                                if (parsed.contains(se)) {
                                    parsed = parsed.substring(0, parsed.indexOf(se));
                                }
                            }
                            parsed = parsed.replace(s, "");
                            if (parsed.indexOf("\\n") < 5 && parsed.indexOf("\\n") != -1) {
                                parsed = parsed.substring(parsed.indexOf("\\n"));
                            } else if (parsed.indexOf("\n") < 5 && parsed.indexOf("\n") != -1) {
                                parsed = parsed.substring(parsed.indexOf("\n"));
                            }
                            parsed = parsed.trim();
                            if (parsed.startsWith("\n") || parsed.startsWith("\\n")) {
                                parsed = parsed.substring(2);
                            }
                            return parsed;
                        }
                    }
                }
            }
            if (parsed == null) {
                String regex = "\\w{3,6}\\d{3,8}\\w{0,2}\\d{0,2}";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(content);
                if (matcher.find()) {
                    for (String s : tokens1) {
                        if (content.contains(s)) {
                            parsed = content.substring(content.indexOf(s), matcher.end());
                            parsed = parsed.substring(0, parsed.lastIndexOf("\n"));
                            parsed = parsed.replace(s, "");
                            if (parsed.startsWith("\n") || parsed.startsWith("\\n")) {
                                parsed = parsed.substring(2);
                            }
                            return parsed;
                        }
                    }
                }
            }
            return parsed;
        } catch (Exception e) {
            log.error("Could not extract 'Address' : ", e.getMessage());
            log.error("Content: {}", content);
            return "";
        }
    }

    private String parseClaveElectorFallback(String content) {
        try {
            String regex = "\\w{3,6}\\d{3,8}\\w{0,2}\\d{0,2}";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(content);
            if (matcher.find()) {
                content = content.substring(matcher.start(), matcher.end());
                return content;
            }
            return "NA";
        } catch (Exception e) {
            log.error("Could not extract 'ClaveElector' in fallback : ", e.getMessage());
            log.error("Content: {}", content);
            return "";
        }
    }

    // @Test
    public void testChar() {
        String test = "IDMEX1599573904くく07810812665969005053H2712310MEX<05<<14546<4";
        String target = replaceASCII(test);
        log.info("test: {} target: {}", test, target);
    }

    private String replaceASCII(String text) {
        String r = text.replaceAll("\\P{InBasic_Latin}", "");
        return r;
    }

    //@Test
    public void test() {
        ResponseEntity<IneDetailDTO> responseEntity = identificationController.findDetail(415l);
        log.info("Response: {}", responseEntity);
    }

    //@Test
    public void testPArse2() {
        String target = "\\nC 12 DE DICIEMBRE 7";
        log.info("Result: {}", target.startsWith("\n"));
        log.info("Result_ {}", target.indexOf("\n"));
    }

    //@Test
    public void testParse1() {
        String response =
                "ESTE DOCUMENTO ES INTRANSFERIBLE,\nNO ES VALIDO SI PRESENTA TACHA-\nOURAS O ENMENDADURAS.\nEL TITULAR ESTA OBLIGADO A NON-\nнCAR PL CAMBIO DE DOMICO EN\nLos 30 DIAS SIGUIENTES A QUE ESTE\nOCURRA.\nSLILLA\n7 0121135290500\nЕрмоо Асово мое\nSECRETAROELECuno be\nINSTITUTO FEDERAL ELECTORAL\nRecienta FEDERALES\nCALES\n";
        log.info("OCR: {}", parseOCRIfe(response));
    }


    private String parseOCRIfe(String content) {
        Pattern pattern = null;
        String regex = "\\d\\d\\d\\d\\d";
        StringBuilder sb = new StringBuilder(regex);
        pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        if (!matcher.find()) {
            return null;
        }
        for (int i = 5; i < 13; i++) {
            sb.append("\\d");
            pattern = Pattern.compile(sb.toString());
            matcher = pattern.matcher(content);
            if (matcher.find()) {
                if (i != 12) {
                    continue;
                } else {
                    int begin = matcher.start();
                    int end = matcher.end();
                    return content.substring(begin, end);
                }
            } else {
                sb.deleteCharAt(sb.length() - 1);
                sb.deleteCharAt(sb.length() - 1);
                sb.deleteCharAt(sb.length() - 1);
                pattern = Pattern.compile(sb.toString());
                matcher = pattern.matcher(content);
                if (matcher.find()) {
                    int begin = matcher.start();
                    int end = matcher.end();
                    return content.substring(begin, end);
                } else {
                    return null;
                }
            }

        }
        return null;
    }

    private void parseCurp(String content) {
        String token1Name = "CURP";
        String tokenName2 = "ANO DE";
        String parsed = parseInfo(content, token1Name, tokenName2);
        log.info("Curp parsed: {}", parsed);
    }

    private void parseAddress(String content) {
        String token1Name = "DOMICILIO";
        String tokenName2 = "CLAVE DE ELECTOR";
        String parsed = parseInfo(content, token1Name, tokenName2);
        log.info("Result: {}", parsed.indexOf("\\n"));
        log.info("Result: {}", parsed.startsWith("\\n"));
        log.info("Result: {}", parsed.indexOf("\n"));
        log.info("Result: {}", parsed.startsWith("\n"));
        log.info("Address parsed: {}", parsed);
    }

    private void parseName(String content) {
        String token1Name = "NOMBRE";
        String tokenName2 = "DOMICILIO";
        String namePreParsed = parseInfo(content, token1Name, tokenName2);
        String[] nameTokens = namePreParsed.split("\\\\n");
        String name = nameTokens[nameTokens.length - 1];
        String apellidoP = null;
        String apellidoM = null;
        int tokenNamesLength = nameTokens.length;
        if (tokenNamesLength > 2) {
            apellidoP = nameTokens[0];
            apellidoM = nameTokens[1];
        } else if (tokenNamesLength == 2) {
            apellidoP = nameTokens[0];
        }
        log.info("Name found: {}", namePreParsed);
        log.info("Name parsed: {} {} {}", name, apellidoP, apellidoM);
    }

    private String parseInfo(String raw, String token1, String token2) {
        int place1 = raw.indexOf(token1);
        int place2 = raw.indexOf(token2);
        raw = raw.substring(place1, place2);
        if (raw.startsWith("\\n")) {
            raw = raw.substring(2);
        }
        return raw;
    }

    //@Test
    public void contextLoads() throws IOException, JSONException {
        byte[] content1 = Files.readAllBytes(Paths.get("/home/amaro/Descargas/anverso.jpeg"));
        byte[] content2 = Files.readAllBytes(Paths.get("/home/amaro/Descargas/reverso.jpeg"));
        List<byte[]> listB = new ArrayList<>();
        listB.add(content1);
        listB.add(content2);
        JSONObject featureRequest = new JSONObject();
        JSONArray featureRequestArray = new JSONArray();
        JSONArray requestsArray = new JSONArray();
        List<JSONObject> listRequest = new ArrayList<>();
        featureRequestArray.put(featureRequest);
        featureRequest.put("type", "TEXT_DETECTION");
        for (byte[] mpf : listB) {
            JSONObject jsonRequest = new JSONObject();
            String b64Img = Base64Utils.encodeToString(mpf);
            JSONObject imageRequest = new JSONObject();
            imageRequest.put("content", "XX");
            jsonRequest.put("image", imageRequest);
            jsonRequest.put("features", featureRequestArray);
            requestsArray.put(jsonRequest);
        }
        JSONObject mainRequest = new JSONObject();
        mainRequest.put("requests", requestsArray);
        log.info("JSON Formed: {}", mainRequest);
    }

}
